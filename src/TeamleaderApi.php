<?php
/**
 * Created by PhpStorm.
 * User: JurgenRolly
 * Date: 14/08/19
 * Time: 17:00
 */

namespace Teamleader;

use Monolog\Logger;
use Monolog\Handler\NativeMailerHandler;

class TeamleaderApi {

  /**
   * @var
   */
  private $access_token;

  /**
   * @var
   */
  private $refresh_token;

  /**
   * @var \Monolog\Logger
   */
  private $logger;

  /**
   * TeamleaderApi constructor.
   * @param \Monolog\Logger $logger
   */
  public function __construct(Logger $logger = null) {
    if ($logger) {
      $this->logger = $logger;
    } else {
      $this->logger = new Logger('teamleaderApi');
      $this->logger->pushHandler(new NativeMailerHandler('jurgen@lab16.be', 'Teamleader logger', 'no-reply@lab16.be', Logger::INFO));
    }
  }

  /**
   * Get connection, use setConnection method when setConnection returns false.
   * This is when access tokens have expired.
   *
   * @param $access_token
   * @return bool
   */
  public function getConnection($access_token) {

    $this->access_token = $access_token;

    // Test connection with the current tokens.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.teamleader.eu/users.me');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->access_token]);
    $response = curl_exec($ch);
    $data = json_decode($response, true);

    if ($data['errors']) {
      $this->logger->error('Get connection: Unable to get a connection: ' . $data['errors'][0]['title']);
      return false;
    }

    $this->logger->info('Get connection: success.');
    return true;

  }

  /**
   * Refresh tokens in order to set a connection.
   *
   * @param $client_id
   * @param $client_secret
   * @param $refresh_token
   * @return array
   */
  public function setConnection($client_id, $client_secret, $refresh_token) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://app.teamleader.eu/oauth2/access_token');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, [
      'client_id' =>  $client_id,
      'client_secret' =>  $client_secret,
      'refresh_token' => $refresh_token,
      'grant_type' => 'refresh_token',
    ]);
    $response = curl_exec($ch);
    $data = json_decode($response, true);

    if ($data['errors']) {
      $this->logger->error('Unable to set a connection: ' . serialize($data));
      return false;
    }
    $this->logger->info('Set connection: success');

    $this->access_token = $data['access_token'];
    $this->refresh_token = $data['refresh_token'];

    // Return the tokens in order to store them on the application level.
    return [
      'access_token' => $data['access_token'],
      'refresh_token' => $data['refresh_token'],
    ];

  }

  /**
   * @param $vat_number
   * @return bool
   */
  public function getCompanyIdByVat($vat_number) {

    $url = 'https://api.teamleader.eu/companies.list';
    $post = [
      'filter' => [
        'term' => $vat_number,
      ]
    ];
    $response = $this->post($url, $post);
    $data = $response['data'];

    if (!empty($data)) {
      $this->logger->info('Get company ID called: Company ' . $data[0]['id']);
      return $data[0]['id'];
    }

    $this->logger->info('Get company ID called: No company found');
    return false;
  }

  /**
   * @param $company_name
   * @param $vat_number
   * @param bool $create
   * @return bool
   */
  // @todo: deprecated method, remove and use getCompanyIdByVat() and createCompany() methods.
  public function getCompanyId($company_name, $vat_number, $create = true) {

    // Check if company exists.
    $url = 'https://api.teamleader.eu/companies.list';
    $post = [
      'filter' => [
        'term' => $vat_number,
      ]
    ];
    $response = $this->post($url, $post);
    $data = $response['data'];

    if (!empty($data)) {
      $this->logger->info('Get company ID called: Company ' . $data['id']);
      return $data['id'];
    }

    $this->logger->info('Get company ID called: No company found');

    if ($create == true) {
      return $this->createCompany($company_name, $vat_number);
    }

    return false;

  }

  /**
   * @param $company_name
   * @param $vat_number
   * @return bool
   */
  public function createCompany($company_name, $vat_number) {
    // Create new company when needed.
    $url = 'https://api.teamleader.eu/companies.add';
    $post = [
      'name' => $company_name,
      'vat_number' => $vat_number,
    ];
    $response = $this->post($url, $post);
    $data = $response['data'];

    if (!empty($data)) {
      $this->logger->info('Create company called: Created company ' . $data['id']);
      return $data['id'];
    }

    $this->logger->error('Create company called: Unable to create company: ' . $response['errors'][0]['title']);

    // Execute a second call with a concatenation of name and VAT number when VAT number check fails.
    if ( $response['errors'][0]['title'] == 'vat_number is an invalid vat number.') {
      $this->logger->error('Create company called: VAT number check failed, executing fallback call.');
      $post = [
        'name' => $company_name . ' - Provided VAT number (unable to validate VAT number): ' . $vat_number,
      ];
      $response = $this->post($url, $post);
      $data = $response['data'];

      if (!empty($data)) {
        $this->logger->info('Create company called: Created company ' . $data['id']);
        return $data['id'];
      }
    }

    return false;
  }

  /**
   * @param $id
   * @return bool
   */
  public function deleteCompany($id) {
    $url = 'https://api.teamleader.eu/companies.delete';
    $post = [
      'id' => $id
    ];

    $response = $this->post($url, $post);

    if ($response['errors']) {
      $this->logger->error('Delete company failed: Unable to delete: ' . $response['errors'][0]['title']);
      return false;
    }

    // @todo: Improve logging, the API method does not return a response but only a 204.
    $this->logger->info('Delete company called: success');
    return true;
  }

  /**
   * @param $email
   * @return bool
   */
  public function getContactIdByEmail($email) {
    $url = 'https://api.teamleader.eu/contacts.list';
    $post = [
      'filter' => [
        'email' => [
          'type' => 'primary',
          'email' => $email,
        ]
      ]
    ];
    $response = $this->post($url, $post);
    $data = $response['data'];

    if (!empty($data)) {
      $this->logger->info('Get contact ID by email called: Contact ' . $data[0]['id']);
      return $data[0]['id'];
    }

    $this->logger->info('Get contact ID by email called: No contact found');
    return false;

  }

  /**
   * @param $email
   * @param $name
   * @param $first_name
   * @param $phone
   * @param bool $create
   * @return bool
   */
  // @todo: deprecated method, remove and use getContactIdByEmail() and createContact() methods.
  public function getContactId($email, $name, $first_name, $phone, $create = true) {

    $url = 'https://api.teamleader.eu/contacts.list';
    $post = [
      'filter' => [
        'email' => [
          'type' => 'primary',
          'email' => $email,
        ]
      ]
    ];
    $response = $this->post($url, $post);
    $data = $response['data'];

    $this->logger->info('Get contact ID called: ' . serialize($data));

    if (!empty($data)) {
      return $data[0]['id'];
    }

    if ($create == true) {
      return $this->createContact($email, $name, $first_name, $phone);
    }

    return false;

  }

  /**
   * @param $email
   * @param $name
   * @param $first_name
   * @param $phone
   * @return bool
   */
  public function createContact($email, $name, $first_name, $phone = null) {
    $url = 'https://api.teamleader.eu/contacts.add';
    $post = [
      'first_name' => $first_name,
      'last_name'  => $name,
      'emails' => [
        [
          'type' => 'primary',
          'email' => $email,
        ]
      ],
    ];

    if ($phone) {
      $post['telephones'] = [
        [
          'type' => 'mobile',
          'number' => $phone,
        ],
      ];
    }

    $response = $this->post($url, $post);
    $data = $response['data'];


    if (!empty($data)) {
      $this->logger->info('Create contact called: Created contact ' . $data['id']);
      return $data['id'];
    }

    $this->logger->error('Create contact called: Unable to create contact: ' . $response['errors'][0]['title']);
    return false;

  }

  /**
   * @param $id
   * @return bool
   */
  public function deleteContact($id) {

    $url = 'https://api.teamleader.eu/contacts.delete';
    $post = [
      'id' => $id
    ];

    $response = $this->post($url, $post);

    if ($response['errors']) {
      $this->logger->error('Delete contact failed: Unable to delete: ' . $response['errors'][0]['title']);
      return false;
    }

    // @todo: Improve logging, the API method does not return a response but only a 204.
    $this->logger->info('Delete contact called: success');
    return true;

  }

  /**
   * @param $contact_id
   * @param $company_id
   * @return bool
   */
  public function linkContactToCompany($contact_id, $company_id) {

    $url = 'https://api.teamleader.eu/contacts.linkToCompany';

    $post = [
      'id' => $contact_id,
      'company_id' => $company_id,
    ];

    $response = $this->post($url, $post);

    if ($response['errors']) {
      $this->logger->error('Link contact to company called: Unable to link: ' . $response['errors'][0]['title']);
      return false;
    }

    // @todo: Improve logging, the API method does not return a response but only a 204.
    $this->logger->info('Link contact to company called: success');
    return true;

  }

  /**
   * @return mixed
   */
  public function getDeal($id) {

    $url = 'https://api.teamleader.eu/deals.info';
    $post = [
      'id' => $id
    ];

    $response = $this->post($url, $post);
    $data = $response['data'];
    $this->logger->info('Get deals called: ' . serialize($data));

    return $data;

  }

  /**
   * @return mixed
   */
  public function getDeals() {

    $url = 'https://api.teamleader.eu/deals.list';
    $post = [];

    $response = $this->post($url, $post);
    $data = $response['data'];
    $this->logger->info('Get deals called: ' . serialize($data));

    return $data;

  }

  /**
   * @param $customer_type
   * @param $customer_id
   * @param $contact_id
   * @param array $custom_fields
   * @return bool
   */
  public function createDeal($customer_type, $customer_id, $contact_id = null, $custom_fields = [], $summary = '') {

    $url = 'https://api.teamleader.eu/deals.create';
    $post = [
      'title' => 'New deal coming from website',
      'summary' => $summary,
      'lead' => [
        'customer' => [
          'type' => $customer_type,
          'id' => $customer_id
        ],
      ],
    ];

    if ($customer_type == 'company' && !is_null($contact_id)) {
      $post['lead']['contact_person_id'] = $contact_id;
    }

    if (!empty($custom_fields)) {
      $post['custom_fields'] = $custom_fields;
    }

    $response = $this->post($url, $post);
    $data = $response['data'];

    if (!empty($data)) {
      $this->logger->info('Create deal called: Created deal ' . $data['id']);
      return $data['id'];
    }

    $this->logger->error('Create deal called: Unable to create deal: ' . $response['errors'][0]['title']);
    return false;

  }

  /**
   * @param $id
   * @return bool
   */
  public function deleteDeal($id) {

    $url = 'https://api.teamleader.eu/deals.delete';
    $post = [
      'id' => $id
    ];

    $response = $this->post($url, $post);

    if ($response['errors']) {
      $this->logger->error('Delete deal failed: Unable to delete: ' . $response['errors'][0]['title']);
      return false;
    }

    // @todo: Improve logging, the API method does not return a response but only a 204.
    $this->logger->info('Delete deal called: success');
    return true;

  }

  /**
   * @return mixed
   */
  public function getCustomFields() {

    $url = 'https://api.teamleader.eu/customFieldDefinitions.list';
    $post = [
      'page' => [
        'size' => 100,
        'number' => 1,
      ],
    ];

    return $this->post($url, $post);

  }

  /**
   * @param $url
   * @param $post
   * @return mixed
   */
  private function post($url, $post) {

    try {
      $ch = curl_init($url);
      $post = json_encode($post);
      $authorization = "Authorization: Bearer " . $this->access_token;
      curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json' ,
        $authorization,
        'X-Api-Version: 2019-07-03',
      ]);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);
      return json_decode($result, true);
    } catch (\Exception $e) {
      $this->logger->error('Teamleader.php post error: ' . $e->getMessage());
    }

  }

}