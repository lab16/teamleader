<?php
/**
 * Created by PhpStorm.
 * User: JurgenRolly
 * Date: 14/08/19
 * Time: 18:30
 */

namespace Teamleader;


class TeamleaderAuth {

  public function authorize($clientId, $redirectUri) {

    $query = [
      'client_id' => $clientId,
      'response_type' => 'code',
      'redirect_uri' => $redirectUri,
    ];

    header('Location: https://app.teamleader.eu/oauth2/authorize?' . http_build_query($query));

  }

  public function getTokens($clientId, $clientSecret, $redirectUri, $code) {

    // @todo: Move to Teamleader class.
    // Obtain the access tokens.
    /**
     * Request an access token and refresh token based on the received authorization code.
     */
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://app.teamleader.eu/oauth2/access_token');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, [
      'code' => $code,
      'client_id' => $clientId,
      'client_secret' => $clientSecret,
      'redirect_uri' => $redirectUri,
      'grant_type' => 'authorization_code',
    ]);
    $response = curl_exec($ch);
    $data = json_decode($response, true);
    if ($data['access_token'] && $data['refresh_token']) {
      // @todo: error handling and logging.
      return [
        'access_token' => $data['access_token'],
        'refresh_token' => $data['refresh_token'],
      ];
    } else {
      // @todo: logging.
    }

  }

}